# Pre-req
 Create volume
```bash
$ docker volume create aldehyde-postgres
```

# Start
Spin up infrastructure
```bash
$ docker-compose up -d
```

# Enter shell
Access shell
```bash
$ docker exec -ti aldehyde bash
```

# Connect to database
Connect to database using psql client
```bash
psql -h db -U postgres
```
